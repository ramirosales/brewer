package com.algaworks.brewer.model;

import java.util.Date;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class Usuario {
	
	@NotBlank(message = "O Nome é obrigatório")
	private String nome;
	
	@Size(min = 1, max = 30, message = "O tamanho do e-mail deve estar entre 1 e 30")
	private String email;
	
	@Size(max = 10, message = "O tamanho da Data de Nascimento deve ser até 10")
	private Date dataNascimento;
	
	@NotBlank(message = "A Senha é obrigatória")
	private String senha;
	
	@Size(max = 1, message = "O tamanho do Status deve ser até 1")
	private String status;
	
	@Size(max = 2, message = "O tamanho dos Grupos deve ser até 2")
	private String grupos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGrupos() {
		return grupos;
	}

	public void setGrupos(String grupos) {
		this.grupos = grupos;
	}

}
