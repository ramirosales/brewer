package com.algaworks.brewer.model;

import javax.validation.constraints.Size;

public class Cidade {

	@Size(min = 1, max = 2, message = "O tamanho do Estado deve estar entre 1 e 2")
	private String estado;
	
	@Size(min = 1, max = 60, message = "O tamanho do Cidade deve estar entre 1 e 60")
	private String cidade;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

}
