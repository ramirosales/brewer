package com.algaworks.brewer.model;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class Cliente {
	
	@NotBlank(message = "Nome é obrigatório")
	private String nome;
	
	@Size(max = 1, message = "O tamanho do Tipo de Pessoa deve ser até 1")
	private String tipoPessoa;
	
	@Size(min = 1, max = 14, message = "O tamanho do CPF/CNPJ deve estar entre 1 e 14")
	private String cpfCnpj;
	
	@Size(max = 20, message = "O tamanho do Telefone deve ser até 20")
	private String telefone;
	
	@Size(max = 30, message = "O tamanho do email deve ser até 30")
	private String email;
	
	@Size(max = 80, message = "O tamanho do Logradouro deve ser até 80")
	private String logradouro;
	
	@Size(max = 10, message = "O tamanho do Número da residência deve ser até 10")
	private String numero;
	
	@Size(max = 80, message = "O tamanho do Complemento deve ser até 80")
	private String complemento;
	
	@Size(max = 10, message = "O tamanho do CEP deve ser até 10")
	private String cep;
	
	@Size(max = 60, message = "O tamanho do Cidade deve ser até 60")
	private String cidade;
	
	@Size(max = 2, message = "O tamanho do Estado deve ser até 2")
	@ManyToOne
	@JoinColumn(name = "codigo_estado")
	private Estado estado;
	
	public Estado getEstado() {
		return estado;
	}
	
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

}
