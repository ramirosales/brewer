package com.algaworks.brewer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.algaworks.brewer.model.TipoPessoa;
import com.algaworks.brewer.repository.Estados;

@Controller
@RequestMapping("/clientes")
public class ClientesController {

	@Autowired
	private Estados estados;

	@RequestMapping("/novo")
	public ModelAndView novo() {
		ModelAndView mv = new ModelAndView("cliente/CadastroCliente");
		mv.addObject("tiposPessoa", TipoPessoa.values());
		mv.addObject("estados", estados.findAll());
		return mv;
	}
		
//	@RequestMapping(value = "/novo", method = RequestMethod.POST)
//	public ModelAndView cadastrar(@Valid Cliente cliente, BindingResult result, Model model, RedirectAttributes attributes) {
//		
//		if (result.hasErrors()) {
//			return novo();
//		}
//		
//		// Salvar no banco de dados
//		
//		attributes.addFlashAttribute("mensagem", "Cliente Salvo com Sucesso !");
//		System.out.println(">>> cadastrar ! Cliente: " + cliente.getNome());
//		return novo(); // "redirect:/clientes/novo";
//	}
	
}
