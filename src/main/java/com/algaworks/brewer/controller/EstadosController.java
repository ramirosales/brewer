package com.algaworks.brewer.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.algaworks.brewer.repository.filter.EstadoFilter;

@Controller
@RequestMapping("/estados")
public class EstadosController {

	//@Autowired
	//private Estados estados;
	
	@GetMapping
	public ModelAndView pesquisar(EstadoFilter estadoFilter, BindingResult result, 
			@PageableDefault(size = 2) Pageable pageable, HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView("estado/PesquisaEstados");
		
		// PageWrapper<Estado> paginaWrapper = new PageWrapper<>(estados.filtrar(estadoFilter, pageable), httpServletRequest);
		//mv.addObject("pagina", paginaWrapper);
		
		return mv;
	}

}
