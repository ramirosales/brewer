CREATE TABLE estilo (
	codigo NUMERIC(20) PRIMARY KEY,
	nome CHARACTER VARYING(50) NOT NULL
);

CREATE SEQUENCE estilo_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE estilo_codigo_seq OWNED BY estilo.codigo;

ALTER TABLE ONLY estilo ALTER COLUMN codigo SET DEFAULT nextval('estilo_codigo_seq'::regclass);



CREATE TABLE cerveja (
	codigo NUMERIC(20) PRIMARY KEY,
	sku CHARACTER VARYING(50) NOT NULL,
	nome CHARACTER VARYING(80) NOT NULL,
	descricao TEXT NOT NULL,
	valor NUMERIC(10, 2) NOT NULL,
	teor_alcoolico NUMERIC(10, 2) NOT NULL,
	comissao NUMERIC(10, 2) NOT NULL,
	sabor CHARACTER VARYING(50) NOT NULL,
	origem CHARACTER VARYING(50) NOT NULL,
	codigo_estilo NUMERIC(20) NOT NULL,
	FOREIGN KEY (codigo_estilo) REFERENCES estilo(codigo)
);

CREATE SEQUENCE cerveja_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE cerveja_codigo_seq OWNED BY cerveja.codigo;

ALTER TABLE ONLY cerveja ALTER COLUMN codigo SET DEFAULT nextval('cerveja_codigo_seq'::regclass);


INSERT INTO estilo (nome) VALUES ('Amber Lager');
INSERT INTO estilo (nome) VALUES ('Dark lager');
INSERT INTO estilo (nome) VALUES ('Pole Lager');
INSERT INTO estilo (nome) VALUES ('Pilsner');

