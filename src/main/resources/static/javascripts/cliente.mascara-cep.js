var Brewer = Brewer || {};

Brewer.MascaraCep = (function() {
	
	function MascaraCep() {
		this.inputCep = $('.js-cep');
	}
	
	MascaraCep.prototype.iniciar = function() {
		this.inputCep.on('change', onCepAlterado.bind(this));
	}
	
	function onCepAlterado(evento) {
		var inputSelecionado = $(evento.currentTarget); 
		this.inputCep.mask(inputSelecionado.data('mascara'));
	}
	
	return MascaraCep;
}());

$(function() {
	var mascaraCep = new Brewer.MascaraCep();
	mascaraCep.iniciar();
});
